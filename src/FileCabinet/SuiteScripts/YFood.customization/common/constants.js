define([], function () {
    return Object.freeze({
        CUSTOM_FIELDS: {
            TRANSACTION_BODY: {
                COGS_CORRECTION_JOURNAL: 'custbody_pdg_cogs_correction_account',
            },
            SEGMENTS: {
                SALES_CHANNEL: 'cseg_xxy_verkaufska',
            },
        },
        CUSTOM_RECORDS: {
            DISCOUNT_ITEM_ACCT_MAPPING: {
                RECORD_ID: 'customrecord_pdg_discount_item_acct_map',
                FIELDS: {
                    DISCOUNT_ITEM: 'custrecord_pdg_discount_item',
                    COGS_CORRECTION_ACCOUNT: 'custrecord_pdg_cogs_correction_account',
                },
            },
        },
        VALUES: {
            COGS_CORRECTION_MEMO: 'COGS correction Journal for Item Fulfillment ',
        },
    });
});