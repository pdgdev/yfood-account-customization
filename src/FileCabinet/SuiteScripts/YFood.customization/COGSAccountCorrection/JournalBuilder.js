/**
 * @NApiVersion 2.1
 */
define([
        'N/record',
        'N/format',
        '../common/constants',
        '../dao/transaction',
    ],
    (
        record,
        format,
        constants,
        transactionDAO
    ) => {

        function JournalBuilder () {
            this.journalEntry = null;
            this.journalEntryId = null;
        }

        JournalBuilder.prototype.createCorrectionJournal = function (itemFulfillment, GLDataForReversal, COGSCorrectionAccount) {
            this.journalEntry = record.create({
                type: record.Type.JOURNAL_ENTRY,
                isDynamic: true,
            });

            let subsidiary = itemFulfillment.getValue('subsidiary');
            let transactionDate = itemFulfillment.getValue('trandate');
            let salesChannel = itemFulfillment.getValue(constants.CUSTOM_FIELDS.SEGMENTS.SALES_CHANNEL);
            let documentNumber = transactionDAO.getUpdatedTransactionNumber(itemFulfillment.id);

            this._setHeaderFields(subsidiary, transactionDate, documentNumber, salesChannel);

            for (let i = 0; i < GLDataForReversal.length; i++) {
                this._createJournalLine('debit', GLDataForReversal[i], COGSCorrectionAccount, documentNumber);
                this._createJournalLine('credit', GLDataForReversal[i], COGSCorrectionAccount, documentNumber);
            }
            this._saveTransaction();

            return this;
        };

        JournalBuilder.prototype.saveJournalReferenceOnSalesOrder = function (salesOrderId) {
            let updateValues = {};
            updateValues[constants.CUSTOM_FIELDS.TRANSACTION_BODY.COGS_CORRECTION_JOURNAL] = this.journalEntryId;

            record.submitFields({
                type: record.Type.SALES_ORDER,
                id: salesOrderId,
                values: updateValues,
            });

            return this;
        }

        JournalBuilder.prototype._setHeaderFields = function (subsidiary, transactionDate, documentNumber, salesChannel) {
            this.journalEntry.setValue({
                fieldId: 'subsidiary',
                value: subsidiary,
            });

            let formattedDate = format.parse({
                value: transactionDate,
                type: format.Type.DATE,
            });

            this.journalEntry.setValue({
                fieldId: 'trandate',
                value: formattedDate,
            });

            this.journalEntry.setValue({
                fieldId: 'memo',
                value: constants.VALUES.COGS_CORRECTION_MEMO + documentNumber,
            });

            this.journalEntry.setValue({
                fieldId: constants.CUSTOM_FIELDS.SEGMENTS.SALES_CHANNEL,
                value: salesChannel,
            })

            return this;
        }

        JournalBuilder.prototype._createJournalLine = function (type, GLLineDataForReversal, COGSCorrectionAccount, documentNumber) {
            this.journalEntry.selectNewLine({
                sublistId: 'line',
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: 'account',
                value: (type === 'credit') ? GLLineDataForReversal.account : COGSCorrectionAccount,
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: type,
                value: GLLineDataForReversal.amount,
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: 'memo',
                value: constants.VALUES.COGS_CORRECTION_MEMO + documentNumber,
            });

            if (GLLineDataForReversal.name) {
                this.journalEntry.setCurrentSublistValue({
                    sublistId: 'line',
                    fieldId: 'entity',
                    value: GLLineDataForReversal.name,
                });
            }

            if (GLLineDataForReversal.subsidiary) {
                this.journalEntry.setCurrentSublistValue({
                    sublistId: 'line',
                    fieldId: 'subsidiary',
                    value: GLLineDataForReversal.subsidiary,
                });
            }

            if (GLLineDataForReversal.department) {
                this.journalEntry.setCurrentSublistValue({
                    sublistId: 'line',
                    fieldId: 'department',
                    value: GLLineDataForReversal.department,
                });
            }

            if (GLLineDataForReversal.class) {
                this.journalEntry.setCurrentSublistValue({
                    sublistId: 'line',
                    fieldId: 'class',
                    value: GLLineDataForReversal.class,
                });
            }

            if (GLLineDataForReversal.location) {
                this.journalEntry.setCurrentSublistValue({
                    sublistId: 'line',
                    fieldId: 'location',
                    value: GLLineDataForReversal.location,
                });
            }

            if (GLLineDataForReversal.salesChannel) {
                this.journalEntry.setCurrentSublistValue({
                    sublistId: 'line',
                    fieldId: constants.CUSTOM_FIELDS.SEGMENTS.SALES_CHANNEL,
                    value: GLLineDataForReversal.salesChannel,
                });
            }

            this.journalEntry.commitLine({
                sublistId: 'line',
            });
        }

        JournalBuilder.prototype._saveTransaction = function () {
            this.journalEntryId = this.journalEntry.save();
            log.debug('<< journalBuilder >>', 'Journal Entry #' + this.journalEntryId + ' has been created.');
        };

        return {Instance: JournalBuilder};
    });