/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */
define([
        '../common/constants'
    ],
    
    (constants) => {
        /**
         * Defines the function definition that is executed before record is loaded.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @param {Form} scriptContext.form - Current form
         * @param {ServletRequest} scriptContext.request - HTTP request information sent from the browser for a client action only.
         * @since 2015.2
         */
        const beforeLoad = (scriptContext) => {
            if (scriptContext.type === scriptContext.UserEventType.COPY) {
                scriptContext.newRecord.setValue({
                    fieldId: constants.CUSTOM_FIELDS.TRANSACTION_BODY.COGS_CORRECTION_JOURNAL,
                    value: ''
                });
            }
        }

        /**
         * Defines the function definition that is executed before record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const beforeSubmit = (scriptContext) => {
            if (scriptContext.type === scriptContext.UserEventType.CREATE) {
                scriptContext.newRecord.setValue({
                    fieldId: constants.CUSTOM_FIELDS.TRANSACTION_BODY.COGS_CORRECTION_JOURNAL,
                    value: ''
                });
            }
        }

        return {beforeLoad, beforeSubmit}

    });
