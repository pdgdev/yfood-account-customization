/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */
define(['N/record', './JournalBuilder', '../dao/transaction'],
    /**
 * @param{record} record
 */
    (record, journalBuilder, transactionDAO) => {

        /**
         * Defines the function definition that is executed after record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const afterSubmit = (scriptContext) => {

            if (!contextAndStatusIsFeasibleForJournalCreation(scriptContext)) {
                return;
            }

            let itemFulfillment = scriptContext.newRecord;
            let itemFulfillmentId = itemFulfillment.id;
            let salesOrderId = itemFulfillment.getValue('createdfrom');

            // Pulls discount item from Sales Order and validates that Journal does not exist yet (returns null if Journal exists)
            let salesOrderDiscountItem = transactionDAO.getDiscountItemFromSalesOrder(salesOrderId);

            // Correction Journal should be generated only when a Sales Order has discount line and Journal does not exist yet
            if (salesOrderDiscountItem) {
                let COGSCorrectionAccount = transactionDAO.getCOGSCorrectionAccount(salesOrderDiscountItem);

                if (COGSCorrectionAccount) {
                    let GLDataForReversal = transactionDAO.getGLDataForReversal(itemFulfillmentId);

                    new journalBuilder.Instance()
                        .createCorrectionJournal(itemFulfillment, GLDataForReversal, COGSCorrectionAccount)
                        .saveJournalReferenceOnSalesOrder(salesOrderId);
                }
            }
        }

        const contextAndStatusIsFeasibleForJournalCreation = (scriptContext) => {
            let itemFulfillmentStatus = scriptContext.newRecord.getValue('shipstatus');

            return (((scriptContext.type === scriptContext.UserEventType.CREATE || scriptContext.type === scriptContext.UserEventType.EDIT) &&
                itemFulfillmentStatus === 'C') || scriptContext.type === scriptContext.UserEventType.SHIP);
        }

        return {afterSubmit}

    });
