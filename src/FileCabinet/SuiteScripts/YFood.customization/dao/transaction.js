define(
    ['N/record', 'N/search', '../common/constants'],
    function (record, search, constants) {

        function getCOGSCorrectionAccount (discountItemId) {
            let discountItemAccountMappingSearch = search.create({
                type: constants.CUSTOM_RECORDS.DISCOUNT_ITEM_ACCT_MAPPING.RECORD_ID,
                filters: [
                    [constants.CUSTOM_RECORDS.DISCOUNT_ITEM_ACCT_MAPPING.FIELDS.DISCOUNT_ITEM, 'anyof', discountItemId]
                ],
                columns: [
                    constants.CUSTOM_RECORDS.DISCOUNT_ITEM_ACCT_MAPPING.FIELDS.COGS_CORRECTION_ACCOUNT
                ]
            });

            let COGSCorrectionAccount;

            discountItemAccountMappingSearch.run().each(function (result) {
                COGSCorrectionAccount = result.getValue(constants.CUSTOM_RECORDS.DISCOUNT_ITEM_ACCT_MAPPING.FIELDS.COGS_CORRECTION_ACCOUNT);
            });

            return COGSCorrectionAccount;
        }

        function getDiscountItemFromSalesOrder (salesOrderId) {
            let salesOrder = record.load({
                type: record.Type.SALES_ORDER,
                id: salesOrderId
            });

            // Prevents from recreating Journal if it exists
            if (salesOrder.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.COGS_CORRECTION_JOURNAL)) {
                log.audit('<< WARNING >>', 'Reversal Journal already exists..');
                return null;
            }

            let discountItem = salesOrder.getValue('discountitem');

            if (discountItem) {
                return discountItem;
            }

            let itemCount = salesOrder.getLineCount({
                sublistId: 'item'
            });

            for (let i = 0; i < itemCount; i++) {
                let currentItemType = salesOrder.getSublistValue({
                    sublistId: 'item',
                    fieldId: 'itemtype',
                    line: i,
                });

                if (currentItemType === 'Discount') {
                    return salesOrder.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'item',
                        line: i,
                    });
                }
            }

            return null;
        }

        function getGLDataForReversal (itemFulfillmentId) {
            let GLImpactSearch = search.create({
                type: search.Type.ITEM_FULFILLMENT,
                filters: [
                    ['internalid', 'anyof', itemFulfillmentId],
                    'AND',
                    ['debitamount', 'isnotempty', '']
                ],
                columns: [
                    'account',
                    'debitamount',
                    'memo',
                    'name',
                    'subsidiary',
                    'department',
                    'class',
                    'location',
                    constants.CUSTOM_FIELDS.SEGMENTS.SALES_CHANNEL,
                ],
            });

            let GLLineData = [];
            let GLLineForReversal = {};

            GLImpactSearch.run().each(function (result) {
                GLLineForReversal = {
                    account: result.getValue('account'),
                    amount: parseFloat(result.getValue('debitamount')),
                    memo: result.getValue('memo'),
                    name: result.getValue('name'),
                    subsidiary: result.getValue('subsidiary'),
                    department: result.getValue('department'),
                    class: result.getValue('class'),
                    location: result.getValue('location'),
                    salesChannel: result.getValue(constants.CUSTOM_FIELDS.SEGMENTS.SALES_CHANNEL),
                };
                GLLineData.push(GLLineForReversal);
                return true;
            });

            return GLLineData;
        }

        function getUpdatedTransactionNumber (transactionId) {
            let transactionObj = search.lookupFields({
                type: search.Type.TRANSACTION,
                id: transactionId,
                columns: ['tranid']
            });

            return transactionObj.tranid;
        }

        return {
            getDiscountItemFromSalesOrder,
            getCOGSCorrectionAccount,
            getGLDataForReversal,
            getUpdatedTransactionNumber,
        }
    });